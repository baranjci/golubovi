package homeprogramming.golubovi;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import homeprogramming.golubovi.view.Prijava;
import homeprogramming.golubovi.view.Registracija;


public class Start extends ActionBarActivity {

    private Button btnRegistracija;
    private Button btnPrijava;
    private Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start);
        Prijava();
        Registracija();
    }

    private void Prijava() {

        btnPrijava = (Button) findViewById(R.id.btnPrijava);
        btnPrijava.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrijavaStart();
            }
        });

    }

    private void Registracija() {

        btnRegistracija = (Button) findViewById(R.id.btnRegistracija);
        btnRegistracija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegistracijaStart();
            }
        });

    }

    private void PrijavaStart() {
        i = new Intent(this, Prijava.class);
        startActivity(i);
    }

    private void RegistracijaStart() {
        i = new Intent(this, Registracija.class);
        startActivity(i);
    }

    private void PostupakRegistracije() {

        new AlertDialog.Builder(this)
                .setMessage("Prilikom registracije popuniti sva polja koja se nalaze na registracijskom obrascu!")
                .show();

    }

    private void Izlaz() {

        new AlertDialog.Builder(this)
                .setTitle("Upozorenje!")
                .setMessage("Želiš li zatvoriti aplikaciju?")
                .setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setIcon(android.R.drawable.ic_delete)
                .show();
    }

    @Override
    public void onBackPressed() {
        Izlaz();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_postupak_registracije:
                PostupakRegistracije();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
