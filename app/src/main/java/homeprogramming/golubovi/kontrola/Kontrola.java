package homeprogramming.golubovi.kontrola;

/**
 * Created by Izidora i Mario on 25.6.2014..
 */
public class Kontrola {

    public static final String BROJ_PRSTENA_PATTERN = "^[a-zA-Z 0-9]{2,250}$";
    public static final String BOJA_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ]{2,50}$";
    public static final String JATO_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ 0-9 ,]{2,250}$";
    public static final String OBJEKT_PATTERN = "^[a-zA-Z ćĆčČđĐšŠžŽ]{2,50}$";
}
