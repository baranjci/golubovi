package homeprogramming.golubovi.tab_view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import homeprogramming.golubovi.R;

/**
 * Created by Mario on 10.1.2015..
 */
public class ListaGolubova extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lista_golubova, container, false);

        return rootView;
    }
}
