package homeprogramming.golubovi.tab_view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import homeprogramming.golubovi.view.ViseOGolubu;

/**
 * Created by Mario on 8.1.2015..
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {

        switch (index) {
            case 0:
                return new ListaGolubova();
            case 1:
                return new ViseOGolubu();

        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
