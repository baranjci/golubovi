package homeprogramming.golubovi.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import homeprogramming.golubovi.R;
import homeprogramming.golubovi.Start;
import homeprogramming.golubovi.kontrola.Kontrola;


public class DodajNovogGoluba extends ActionBarActivity {

    private EditText txtBrojPrstena;
    private EditText txtBoja;
    private EditText txtJato;
    private EditText txtObjekt;
    private Pattern pattern;
    private Matcher matcher;
    private Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dodaj_novog_goluba);
        DodajNovogGoluba();
    }

    private void DodajNovogGoluba() {
        txtBrojPrstena = (EditText) findViewById(R.id.txtBrojPrstena);
        txtBoja = (EditText) findViewById(R.id.txtBoja);
        txtJato = (EditText) findViewById(R.id.txtJato);
        txtObjekt = (EditText) findViewById(R.id.txtObjekt);

        final Button rbtnMuski = (Button) findViewById(R.id.rbtnMuski);
        final Button rbtnZenski = (Button) findViewById(R.id.rbtnZenski);
        final Button btnSlikaj = (Button) findViewById(R.id.btnSlikaj);
        final Button btnPonistiUnos = (Button) findViewById(R.id.btnPonistiUnos);
        final Button btnSpremi = (Button) findViewById(R.id.btnSpremi);
        final RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        final Button btnSpremljeniGolubovi = (Button) findViewById(R.id.btnSpremljeniGolubovi);

        btnSpremljeniGolubovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StartSpremljeni();
            }
        });

        btnSpremi.setEnabled(false);

        try {
            btnPonistiUnos.setOnClickListener(new View.OnClickListener() {

                public void onClick(View view) {
                    txtBrojPrstena.setText("");
                    txtJato.setText("");
                    txtBoja.setText("");
                    txtObjekt.setText("");
                    radioGroup.setSelected(false);
                    rbtnMuski.setSelected(false);
                    rbtnZenski.setSelected(false);
                    txtBrojPrstena.requestFocus();
                }

            });

        } catch (NullPointerException e) {

        }


        rbtnMuski.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Odabran je muški spol!",
                        Toast.LENGTH_SHORT).show();
                if (rbtnMuski.isSelected()) {
                    rbtnZenski.setSelected(false);
                }

            }
        });

        rbtnZenski.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Toast.makeText(getApplicationContext(), "Odabran je ženski spol!",
                        Toast.LENGTH_SHORT).show();
                if (rbtnZenski.isSelected()) {
                    rbtnMuski.setSelected(false);
                }
            }
        });


        btnSlikaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                startActivityForResult(intent, 0);
            }
        });

        btnSpremi.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                if (!Kontrola()) {
                    errorMessage();
                    return;
                } else {
                    Toast.makeText(getApplicationContext(), "Unos je ispravan!",
                            Toast.LENGTH_SHORT).show();
                    Spremi();
                    radioGroup.setSelected(false);
                    rbtnMuski.setSelected(false);
                    rbtnZenski.setSelected(false);
                }

            }

            private boolean rbtnKontrola() {

                if (!rbtnMuski.isSelected()) {
                    if (!rbtnZenski.isSelected()) {
                        rbtnError();
                        return false;
                    }
                }
                return true;
            }
        });


        txtBrojPrstena.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                btnSpremi.setEnabled(!txtBrojPrstena.getText().toString().trim().isEmpty());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (txtBoja.getText().toString().trim().isEmpty() && txtObjekt.getText().toString().trim().isEmpty()
                        && txtJato.getText().toString().trim().isEmpty()) {
                    btnSpremi.setEnabled(false);
                }

            }
        });

        txtBoja.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                btnSpremi.setEnabled(!txtBoja.getText().toString().trim().isEmpty());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (txtObjekt.getText().toString().trim().isEmpty()
                        && txtJato.getText().toString().trim().isEmpty()) {
                    btnSpremi.setEnabled(false);
                }

            }
        });

        txtObjekt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                btnSpremi.setEnabled(!txtObjekt.getText().toString().trim().isEmpty());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (txtJato.getText().toString().trim().isEmpty()) {
                    btnSpremi.setEnabled(false);
                }

            }
        });

        txtJato.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                btnSpremi.setEnabled(!txtJato.getText().toString().trim().isEmpty());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (txtObjekt.getText().toString().trim().isEmpty()) {
                    btnSpremi.setEnabled(false);
                }
            }
        });

    }

    private boolean Kontrola() {

        if (!KontrolaBrojPrstena()) {
            return false;
        }
        if (!KontrolaBoja()) {
            return false;
        }
        if (!KontrolaJato()) {
            return false;
        }
        if (!KontrolaObjekt()) {
            return false;
        }
        return true;
    }

    private void Spremi() {

        txtObjekt.setText("");
        txtBoja.setText("");
        txtJato.setText("");
        txtBrojPrstena.setText("");
        txtBrojPrstena.requestFocus();

    }

    private boolean KontrolaBrojPrstena() {

        pattern = Pattern.compile(Kontrola.BROJ_PRSTENA_PATTERN);
        matcher = pattern.matcher(txtBrojPrstena.getText().toString().trim());

        if (!matcher.matches()) {
            txtBrojPrstena.requestFocus();
            txtBrojPrstena.setBackgroundColor(Color.RED);
            return false;
        }
        txtBrojPrstena.setBackgroundColor(Color.TRANSPARENT);
        return true;
    }

    private boolean KontrolaBoja() {

        pattern = Pattern.compile(Kontrola.BOJA_PATTERN);
        matcher = pattern.matcher(txtBoja.getText().toString().trim());
        if (!matcher.matches()) {
            txtBoja.requestFocus();
            txtBoja.setBackgroundColor(Color.RED);
            return false;
        }
        txtBoja.setBackgroundColor(Color.TRANSPARENT);
        return true;
    }

    private boolean KontrolaJato() {

        pattern = Pattern.compile(Kontrola.JATO_PATTERN);
        matcher = pattern.matcher(txtJato.getText().toString().trim());
        if (!matcher.matches()) {
            txtJato.requestFocus();
            txtJato.setBackgroundColor(Color.RED);
            return false;
        }
        txtJato.setBackgroundColor(Color.TRANSPARENT);
        return true;
    }

    private boolean KontrolaObjekt() {

        pattern = Pattern.compile(Kontrola.OBJEKT_PATTERN);
        matcher = pattern.matcher(txtObjekt.getText().toString().trim());
        if (!matcher.matches()) {
            txtObjekt.requestFocus();
            txtObjekt.setBackgroundColor(Color.RED);
            return false;
        }
        txtObjekt.setBackgroundColor(Color.TRANSPARENT);
        return true;
    }

    private void errorMessage() {

        new AlertDialog.Builder(this)
                .setTitle("Greška!")
                .setMessage("Unos nije ispravan!")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })

                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void rbtnError() {

        new AlertDialog.Builder(this)
                .setTitle("Greška!")
                .setMessage("Odaberi spol unesenog goluba!")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })

                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void StartSpremljeni() {
        i = new Intent(this, SpremljeniGolubovi.class);
        startActivity(i);
    }

    private void Start() {
        i = new Intent(this, Start.class);
        startActivity(i);
    }

    private void Izlaz() {

        new AlertDialog.Builder(this)
                .setTitle("Upozorenje!")
                .setMessage("Želiš li zatvoriti aplikaciju?")
                .setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
                        startActivity(intent);
                        finish();
                    }
                })
                .setNegativeButton("Ne", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        return;
                    }
                })
                .setIcon(android.R.drawable.ic_delete)
                .show();
    }

    @Override
    public void onBackPressed() {
        Izlaz();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_odjava:
                Start();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
