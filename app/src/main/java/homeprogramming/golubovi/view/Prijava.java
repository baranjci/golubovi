package homeprogramming.golubovi.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import homeprogramming.golubovi.R;

/**
 * Created by Mario on 6.1.2015..
 */
public class Prijava extends ActionBarActivity {

    private EditText korisnicko_ime;
    private EditText lozinka;
    private Button btnPrijava;
    private Intent i;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prijava);
        PrijaviSe();
    }

    private void PrijaviSe() {

        btnPrijava = (Button) findViewById(R.id.btnPrijava);
        btnPrijava.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Prijava.this, "Ispravno uneseni korisničko ime i lozinka", Toast.LENGTH_SHORT).show();
                PocistiPolja();
                StartDodajNovog();
            }
        });
    }

    private void PocistiPolja() {

        korisnicko_ime = (EditText) findViewById(R.id.txtKorisnickoIme);
        lozinka = (EditText) findViewById(R.id.txtLozinka);

        korisnicko_ime.setText(" ");
        lozinka.setText(" ");
    }

    private void StartDodajNovog() {
        i = new Intent(this, DodajNovogGoluba.class);
        startActivity(i);
    }

}
