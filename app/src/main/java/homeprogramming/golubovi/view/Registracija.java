package homeprogramming.golubovi.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import homeprogramming.golubovi.R;

/**
 * Created by Mario on 6.1.2015..
 */
public class Registracija extends ActionBarActivity {

    private EditText korisnicko_ime;
    private EditText lozinka;
    private EditText ime;
    private EditText prezime;
    private EditText adresa;
    private EditText email;
    private Button btnRegistracija;
    private Intent i;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registracija);
        RegistrirajSe();
    }

    private void RegistrirajSe() {

        btnRegistracija = (Button) findViewById(R.id.btnRegistracija);
        btnRegistracija.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(Registracija.this, "Ispravno uneseni korisničko ime i lozinka", Toast.LENGTH_SHORT).show();
                PocistiPolja();
                StartDodajNovog();
            }
        });

    }

    private void PocistiPolja() {

        korisnicko_ime = (EditText) findViewById(R.id.txtKorisnickoIme);
        lozinka = (EditText) findViewById(R.id.txtLozinka);
        ime = (EditText) findViewById(R.id.txtIme);
        prezime = (EditText) findViewById(R.id.txtPrezime);
        adresa = (EditText) findViewById(R.id.txtAdresa);
        email = (EditText) findViewById(R.id.txtEmail);

        korisnicko_ime.setText(" ");
        lozinka.setText(" ");
        ime.setText(" ");
        prezime.setText(" ");
        adresa.setText(" ");
        email.setText(" ");
    }

    private void StartDodajNovog() {
        i = new Intent(this, DodajNovogGoluba.class);
        startActivity(i);
    }
}
