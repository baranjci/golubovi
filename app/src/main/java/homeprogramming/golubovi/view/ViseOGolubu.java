package homeprogramming.golubovi.view;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import homeprogramming.golubovi.R;

/**
 * Created by Mario on 10.1.2015..
 */
public class ViseOGolubu extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.vise_o_golubu, container, false);

        return rootView;
    }
}
